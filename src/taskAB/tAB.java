package taskAB;
import java.util.*;
import java.io.*;

public class tAB{ //имя класса должно быть Main
  public static void main(String[] argv) throws IOException{
    //new tAB().run();
	  PrintWriter pw;
	  //String data = readData("input.txt");
	  	String data = readData("input.txt");
		String[] dataArr = data.split(" ");
		int a = Integer.parseInt(dataArr[0]);
		int b = Integer.parseInt(dataArr[1]);
	    //pw = new PrintWriter(new File("output.txt"));
	    //pw.print(a+b);
	    //pw.close();
		writeResult(String.valueOf(a + b), "output.txt");
  }
 
  /*public void run() throws IOException{
   
  }*/
  public static String readData(String filePath){
		
		BufferedReader bufferedReader = null;
		FileReader fileReader = null;
		String resultString = "";
		
		File file = new File(filePath);
		
		if (file.exists()) {

			if((file.length()/1024) < 256){

				try {
					
					fileReader = new FileReader(filePath);
	    			bufferedReader = new BufferedReader(fileReader);
	    			
	    			String currentLine;
	    			int lineNumber = 1;
	    			
	    			while ((currentLine = bufferedReader.readLine()) != null) {
	    				
	    				return currentLine;
	    				
	    			}
					
				} catch (IOException e) {
	    			
	    			//e.printStackTrace();

	    		} finally {

	    			try {

	    				if (bufferedReader != null) {
	    					
	    					bufferedReader.close();
	    				}
	    					
	    				if (fileReader != null) {
	    					
	    					fileReader.close();
	
	    				}
	    					

	    			} catch (IOException ex) {

	    				
	
	    				//ex.printStackTrace();
	    			}

	    		}
				
			} else {
				
				
			}
			
		} else {
			
			
		}
		
		return resultString;
	}
  	
  public static void writeResult(String resultAnswer, String filePath) {
		
		String filePathCreated = filePath.replaceAll("INPUT.TXT", "OUTPUT.TXT");
		
		
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;

		try {
		
			fileWriter  = new FileWriter(filePathCreated);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(resultAnswer);
			bufferedWriter.write("\n");
			
			System.out.println("Result recorded into file (" + filePathCreated + ") successfully.");

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (bufferedWriter != null) {
					
					bufferedWriter.close();
				}					

				if (fileWriter  != null) {
					
					fileWriter .close();
					System.out.println("File (" + filePathCreated + ") closed successfully.\n");
				}
					
			} catch (IOException ex) {
				
				System.out.println("Error close file (" +  filePathCreated + ")\n");
				ex.printStackTrace();

			}

		}
		
		
	}
}