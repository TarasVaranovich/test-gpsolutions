package taskAB;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class taskAB {
	
	public static void main(String[] args) throws IOException {

		if(args.length != 0) { 
			
			String filePathExisting = args[0];
			int index = filePathExisting.lastIndexOf('.');
			PrintWriter pw;
			
			if(index != -1 && filePathExisting.substring(index).equalsIgnoreCase(".TXT")){

				String data = readData( filePathExisting);
				String[] dataArr = data.split(" ");
				int a = Integer.parseInt(dataArr[0]);
				int b = Integer.parseInt(dataArr[1]);
				int c = 0;
				if(a <= 1000000000 && b <= 1000000000){

					//writeResult(c, filePathExisting);
					try{
						 pw = new PrintWriter(new File("output.txt"));
						 pw.print(a+b);
						 pw.close();
					} catch(Exception e) {
						//e.printStackTrace();
					}
					 
				} else {
					//
				}	
				
				
			} else {
				
				
			}
			
		} else {
			
				
		}
	}
	
	public static String readData(String filePath){
		
		BufferedReader bufferedReader = null;
		FileReader fileReader = null;
		String resultString = "";
		
		File file = new File(filePath);
		
		if (file.exists()) {

			if((file.length()/1024) < 256){

				try {
					
					fileReader = new FileReader(filePath);
	    			bufferedReader = new BufferedReader(fileReader);
	    			
	    			String currentLine;
	    			int lineNumber = 1;
	    			
	    			while ((currentLine = bufferedReader.readLine()) != null) {
	    				
	    				return currentLine;
	    				
	    			}
					
				} catch (IOException e) {
	    			
	    			//e.printStackTrace();

	    		} finally {

	    			try {

	    				if (bufferedReader != null) {
	    					
	    					bufferedReader.close();
	    				}
	    					
	    				if (fileReader != null) {
	    					
	    					fileReader.close();
	
	    				}
	    					

	    			} catch (IOException ex) {

	    				
	
	    				//ex.printStackTrace();
	    			}

	    		}
				
			} else {
				
				
			}
			
		} else {
			
			
		}
		
		return resultString;
	}
	
	public static void writeResult(int resultAnswer, String filePath) {
		
		String filePathCreated = filePath.replaceAll("INPUT.TXT", "output.txt");		
		
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;

		try {
		
			fileWriter  = new FileWriter(filePathCreated);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(resultAnswer + "");
			bufferedWriter.close();

		} catch (IOException e) {

			//e.printStackTrace();

		} finally {

			try {

				if (bufferedWriter != null) {
					
					bufferedWriter.close();
				}					

				if (fileWriter  != null) {
					
					fileWriter .close();

				}
					
			} catch (IOException ex) {
				
				//ex.printStackTrace();

			}

		}
		
		
	}
}
