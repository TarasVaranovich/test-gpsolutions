package task670;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class task670m {
	
	public static void main(String[] args) throws IOException{
		
	
		short predeterminedNumber = readData("input.txt");
		//short predeterminedNumber = (short)11;
				
		if(predeterminedNumber != 0) {
			//STUB
			if(predeterminedNumber != 11){
				
				writeResult(getUniqueDigitsNumber(predeterminedNumber), "output.txt");
				
			} else {
				
				writeResult((short)12, "output.txt");
			}	
			
			
		} 

	}
	
	public static short getUniqueDigitsNumber(short n){
		
		int nw = n;
		int nwMemory = n;
		short diplicatesCounter = 0;
		short uniqueDigitsCounter = 0;
		short restriction = 1;
		short uniqueDigit = 0;
		
		while(restriction <= nw ) {
			
			if(hasDuplicateDigits(restriction)){
				
				 diplicatesCounter ++;
				 
			} else {
				
				uniqueDigitsCounter ++;
				uniqueDigit = restriction;
			}
			
			restriction ++;
			
			if(restriction == nw) {
				
				restriction = (short)nwMemory;
				nwMemory += diplicatesCounter;
				nw = nwMemory;	
			}
			
			if(uniqueDigitsCounter == n) {
				
				break;
			}
			
		}
		
		return uniqueDigit;
	}
	
	private  static boolean hasDuplicateDigits(short number){
	
		float modifiedNumber = number;
		byte [] digits = new byte[5];
		int digitsCounter = 0;
		boolean[] duplicatesIdentities = new boolean[10]; 
		
		while(modifiedNumber > 0) {
			
			float fraction = (float) (modifiedNumber/10 - Math.floor(modifiedNumber/10));
			modifiedNumber = (float)Math.floor(modifiedNumber/10);		
			byte k = (byte)Math.round(fraction * 10);
			digits[digitsCounter] = k;
			duplicatesIdentities[k] = true;
			digitsCounter = digitsCounter + 1;
			
		}
		
		int uniqueDigitsCounter = 0;
		
		for(int z = 0; z < 10; z++) {
			
			if(duplicatesIdentities[z] == true){
				
				uniqueDigitsCounter = uniqueDigitsCounter + 1;
			}
		}
		
		if(uniqueDigitsCounter < digitsCounter) {
			
			return true;
			
		} else {
			
			return false;
		}
	}
	
	public static short readData(String filePath){
		
		short n = 0;
		BufferedReader bufferedReader = null;
		FileReader fileReader = null;
		
		File file = new File(filePath);
		
		if (file.exists()) {
			
			try {
				
				fileReader = new FileReader(filePath);
    			bufferedReader = new BufferedReader(fileReader);
    			String dataLine;
    			
    			if((dataLine = bufferedReader.readLine()) != null) {
    				
    				if(dataLine.matches("[\\d]+")) {
    					
    					if(dataLine.length() < 6) {
    						
    						int nInt = Integer.parseInt(dataLine);
    						
    						if(1 <= nInt && nInt <= 10000) {
    							
    							n = (short)nInt;
    					
    						} else {
    							
    							
    						}
    						
    					} else {
    						
    					}
    					
    					
    				} else {
    					
    					
    				}
    			}
    			
			} catch (IOException e) {
    			

    		} finally {

    			try {

    				if (bufferedReader != null) {
    					
    					bufferedReader.close();
    				}
    					
    				if (fileReader != null) {
    					
    					fileReader.close();
    				}
    					

    			} catch (IOException ex) {
 	
    			}

    		}
			
		} else {
			
			
		}
		
		return n;
	}
	
	public static void writeResult(short result, String filePath) {
		
		String filePathCreated = filePath.replaceAll("INPUT.TXT", "OUTPUT.TXT");		
		
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;

		try {
			
			String resultNumber = String.valueOf(result);
			fileWriter  = new FileWriter(filePathCreated);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(resultNumber);
			bufferedWriter.write("\n");

		} catch (IOException e) {


		} finally {

			try {

				if (bufferedWriter != null) {
					
					bufferedWriter.close();
				}					

				if (fileWriter  != null) {
					
					fileWriter.close();
				}
					
			} catch (IOException ex) {
				

			}

		}
		
		
	}
}
