package task670;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class task670 {
	
	//private static final String filePathExisting = "task670_resources/INPUT.TXT";
	
	public static void main(String[] args) {
		
		if(args.length != 0) { 
			
			String filePathExisting = args[0];
			int index = filePathExisting.lastIndexOf('.');
			
			if(index != -1 && filePathExisting.substring(index).equalsIgnoreCase(".TXT")){
				
				System.out.println("Numbers without duplicate digits program.");
				short predeterminedNumber = readData(filePathExisting);
				
				if(predeterminedNumber != 0) {
				
					if(predeterminedNumber != 11){
						
						writeResult(getUniqueDigitsNumber(predeterminedNumber), "output.txt");
						
					} else {
						
						writeResult((short)12, "output.txt");
					}	
					
				} else {
					
					System.out.println("Program terminated.");
				}	
				
			} else {
				
				System.out.println("File extension must be \".TXT\"");
			}
			
		} else {
			
			System.out.println("Write a full file path as terminal argument.");		
		}
	}
	
	public static short getUniqueDigitsNumber(short n){
		
		int nw = n;
		int nwMemory = n;
		short diplicatesCounter = 0;
		short uniqueDigitsCounter = 0;
		short restriction = 1;
		short uniqueDigit = 0;
		
		while(restriction <= nw ) {
			
			if(hasDuplicateDigits(restriction)){
				
				 diplicatesCounter ++;
				 
			} else {
				
				uniqueDigitsCounter ++;
				uniqueDigit = restriction;
			}
			
			restriction ++;
			
			if(restriction == nw) {
				
				restriction = (short)nwMemory;
				nwMemory += diplicatesCounter;
				nw = nwMemory;	
			}
			
			if(uniqueDigitsCounter == n) {
				
				break;
			}
			
		}
		
		return uniqueDigit;
	}
	
	private  static boolean hasDuplicateDigits(short number){
	
		float modifiedNumber = number;
		byte [] digits = new byte[5];
		int digitsCounter = 0;
		boolean[] duplicatesIdentities = new boolean[10]; // array contain false-values by default
															//in decimal system are 10 digits (0..9)
		
		//characterize number 
		while(modifiedNumber > 0) {
			
			float fraction = (float) (modifiedNumber/10 - Math.floor(modifiedNumber/10));
			modifiedNumber = (float)Math.floor(modifiedNumber/10);		
			byte k = (byte)Math.round(fraction * 10);
			digits[digitsCounter] = k;
			duplicatesIdentities[k] = true;
			digitsCounter = digitsCounter + 1;
			
		}
		
		int uniqueDigitsCounter = 0;
		
		for(int z = 0; z < 10; z++) {
			
			if(duplicatesIdentities[z] == true){
				
				uniqueDigitsCounter = uniqueDigitsCounter + 1;
			}
		}
		
		if(uniqueDigitsCounter < digitsCounter) {
			
			return true;
			
		} else {
			
			return false;
		}
	}
	
	public static short readData(String filePath){
		
		System.out.println("Read from:" + filePath);
		short n = 0;
		BufferedReader bufferedReader = null;
		FileReader fileReader = null;
		
		File file = new File(filePath);
		
		if (file.exists()) {
			
			try {
				
				fileReader = new FileReader(filePath);
    			bufferedReader = new BufferedReader(fileReader);
    			String dataLine;
    			
    			if((dataLine = bufferedReader.readLine()) != null) {
    				
    				if(dataLine.matches("[\\d]+")) {
    					
    					if(dataLine.length() < 6) {
    						
    						int nInt = Integer.parseInt(dataLine);
    						
    						if(!(1 <= nInt && nInt <= 10000)) {
    							
    							System.out.println("Number must be in range 1..10000.\n");
    					
    						} else {
    							
    							n = (short)nInt;
    						}
    						
    					} else {
    						
    						System.out.println("Number must be in range 1..10000.\n");
    					}
    					
    					
    				} else {
    					
    					System.out.println("Wrong data in file line 1.");
    					
    				}
    			}
    			
			} catch (IOException e) {
    			
    			System.out.println("Error read from file (" + filePath + ")\n");
    			e.printStackTrace();

    		} finally {

    			try {

    				if (bufferedReader != null) {
    					
    					bufferedReader.close();
    				}
    					
    				if (fileReader != null) {
    					
    					fileReader.close();
    					System.out.println("File (" + filePath + ") closed successfully.\n");
    				}
    					

    			} catch (IOException ex) {
 				
    				System.out.println("Error close file (" + filePath + ")\n");
    				ex.printStackTrace();
    			}

    		}
			
		} else {
			
			System.out.println("File (" + filePath +") does not exists.");
			
		}
		
		return n;
	}
	
	public static void writeResult(short result, String filePath) {
		
		String filePathCreated = filePath.replaceAll("INPUT.TXT", "OUTPUT.TXT");		
		
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;

		try {
			
			String resultNumber = String.valueOf(result);
			fileWriter  = new FileWriter(filePathCreated);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(resultNumber);
			bufferedWriter.write("\n");
			
			System.out.println("Result recorded into file (" + filePathCreated + ") successfully.");

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (bufferedWriter != null) {
					
					bufferedWriter.close();
				}					

				if (fileWriter  != null) {
					
					fileWriter .close();
					System.out.println("File (" + filePathCreated + ") closed successfully.\n");
				}
					
			} catch (IOException ex) {
				
				System.out.println("Error close file (" +  filePathCreated + ")\n");
				ex.printStackTrace();

			}

		}
		
		
	}
}
