package task278;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class task278 {
	
	//private static final String filePathExisting = "task278_resources/INPUT.TXT";
	
	public static void main(String[] args) {
		
		if(args.length != 0) { 

			String filePathExisting = args[0];
			int index = filePathExisting.lastIndexOf('.');
			
			if(index != -1 && filePathExisting.substring(index).equalsIgnoreCase(".TXT")){
				
				System.out.println("Computational biology program.");
				String predeterminedString = readData(filePathExisting);
				
				if(predeterminedString.length() > 0) {
						
					String[] dataArray = predeterminedString.split(" ");
						
					try{
							
						byte[] dna = dataArray[0].getBytes();
						byte[] evolved = dataArray[1].getBytes();
				
						if(dna.length <= evolved.length){
							
							writeResult(isEvolved(dna, evolved), "output.txt");
							
						} else {
							writeResult("NO", "output.txt");
						}
							
					} catch(Exception e) {
							
						System.out.println("Problem with parsing data.");
					}
					
				} else {
						
					System.out.println("Program terminated.");
				}
				
				
			} else {
				
				System.out.println("File extension must be \".TXT\"");		
			}			
			
		} else {
			
			System.out.println("Write a full file path as terminal argument.");		
		}
	}
	
	public static String isEvolved(byte[] dna, byte[] evolved) {
		
		int ndna = dna.length - 1;
		int nevolved = evolved.length - 1;
					
		while(nevolved >= 0 && ndna >= 0){
				
			if(evolved[nevolved] == dna[ndna]){
					
				ndna--;
				nevolved--;
					
			} else {
					
				nevolved--;
			}
			
		}
			
		if(ndna == -1) {
	
			return "YES";
				
		} else {
	
			return "NO";
		}
	}
	
	public static String readData(String filePath){
		
		System.out.println("Read from:" + filePath);
		BufferedReader bufferedReader = null;
		FileReader fileReader = null;
		String resultString = "";
		
		File file = new File(filePath);
		
		if (file.exists()) {

			if((file.length()/1024) < 256){

				try {
					
					fileReader = new FileReader(filePath);
	    			bufferedReader = new BufferedReader(fileReader);
	    			
	    			String currentLine;
	    			int lineNumber = 1;
	    			
	    			while ((currentLine = bufferedReader.readLine()) != null) {
	    				
	    				if(lineNumber == 1) {
	    					
	    					if(currentLine.matches("[\\'A'?\\'G'?\\'C'?\\'T'?]+") && currentLine.length() > 0){
	    						
	    						resultString = resultString.concat(currentLine);
	    						
	    					} else {
	    						
	    						System.out.println("Wrong data in file line 1.\n" +
	    											"File must contain only A, G, C, T symbols and at least one of them per "
	    											+ "string.");
	    						
	    						break;
	    					}
	    					
	    				} else if (lineNumber == 2) {
	    					
	    					if(currentLine.matches("[\\'A'?\\'G'?\\'C'?\\'T'?]+")){
	    						
	    							
	    						resultString = resultString.concat(" ");
	    						resultString = resultString.concat(currentLine);	    						
	    										
	    					} else {
	    						
	    						System.out.println("Wrong data in file line 2.\n" +
										"File must contain only A, G, C, T symbols and at least one of them per string.");
	    						resultString = "";
	    						
	    						break;
	    						
	    					}
	    					
	    				} else {
	    					
	    					break;
	    				}
	    				
	    				lineNumber ++;
	    			}
					
				} catch (IOException e) {
	    			
	    			System.out.println("Error read from file (" + filePath + ")\n");
	    			e.printStackTrace();

	    		} finally {

	    			try {

	    				if (bufferedReader != null) {
	    					
	    					bufferedReader.close();
	    				}
	    					
	    				if (fileReader != null) {
	    					
	    					fileReader.close();
	    					System.out.println("File (" + filePath + ") closed successfully.\n");
	    				}
	    					

	    			} catch (IOException ex) {

	    				
	    				System.out.println("Error close file (" + filePath + ")\n");
	    				ex.printStackTrace();
	    			}

	    		}
				
			} else {
				
				System.out.println("File (" + filePath +") size is more than 256 kilobytes.");
			}
			
		} else {
			
			System.out.println("File (" + filePath +") does not exists.");
		}
		
		return resultString;
	}
	
	public static void writeResult(String resultAnswer, String filePath) {
		
		String filePathCreated = filePath.replaceAll("INPUT.TXT", "OUTPUT.TXT");
		
		
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;

		try {
		
			fileWriter  = new FileWriter(filePathCreated);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(resultAnswer);
			bufferedWriter.write("\n");
			
			System.out.println("Result recorded into file (" + filePathCreated + ") successfully.");

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (bufferedWriter != null) {
					
					bufferedWriter.close();
				}					

				if (fileWriter  != null) {
					
					fileWriter .close();
					System.out.println("File (" + filePathCreated + ") closed successfully.\n");
				}
					
			} catch (IOException ex) {
				
				System.out.println("Error close file (" +  filePathCreated + ")\n");
				ex.printStackTrace();

			}

		}
		
		
	}
}
