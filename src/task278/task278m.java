package task278;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class task278m {
	
	
	public static void main(String[] args) throws IOException{
		
		String predeterminedString = readData("input.txt");
		if(predeterminedString.length() > 0) {
			
			String[] dataArray = predeterminedString.split(" ");
			
			byte[] dna = dataArray[0].getBytes();
			byte[] evolved = dataArray[1].getBytes();
			if(dna.length <= evolved.length){
				writeResult(isEvolved(dna, evolved), "output.txt");
			} else {
				writeResult("NO", "output.txt");
			}
			
		}							
	}
	
	public static String isEvolved(byte[] dna, byte[] evolved) {
		
		int ndna = dna.length - 1;
		int nevolved = evolved.length - 1;
					
		while(nevolved >= 0 && ndna >= 0){
				
			if(evolved[nevolved] == dna[ndna]){
					
				ndna--;
				nevolved--;
					
			} else {
					
				nevolved--;
			}
			
		}
			
		if(ndna == -1) {
	
			return "YES";
				
		} else {
	
			return "NO";
		}
	}
	
	public static String readData(String filePath){
		
		BufferedReader bufferedReader = null;
		FileReader fileReader = null;
		String resultString = "";
		
		File file = new File(filePath);
		
		if (file.exists()) {

			if((file.length()/1024) <= 256){

				try {
					
					fileReader = new FileReader(filePath);
	    			bufferedReader = new BufferedReader(fileReader);
	    			
	    			String currentLine;
	    			int lineNumber = 1;
	    			
	    			while ((currentLine = bufferedReader.readLine()) != null) {
	    				
	    				if(lineNumber == 1) {
	    					
	    					if(currentLine.matches("[\\'A'?\\'G'?\\'C'?\\'T'?]+") && currentLine.length() > 0){
	    						
	    						resultString = resultString.concat(currentLine);
	    						
	    					} else {
	    						
	    						
	    						break;
	    					}
	    					
	    				} else if (lineNumber == 2) {
	    					
	    					if(currentLine.matches("[\\'A'?\\'G'?\\'C'?\\'T'?]+")){
	    						
	    							
	    						resultString = resultString.concat(" ");
	    						resultString = resultString.concat(currentLine);
		    						
	    										
	    					} else {
	    						
	    						resultString = "";
	    						
	    						break;
	    						
	    					}
	    					
	    				} else {
	    					
	    					break;
	    				}
	    				
	    				lineNumber ++;
	    			}
					
				} catch (IOException e) {
	    			

	    		} finally {

	    			try {

	    				if (bufferedReader != null) {
	    					
	    					bufferedReader.close();
	    				}
	    					
	    				if (fileReader != null) {
	    					
	    					fileReader.close();

	    				}
	    					

	    			} catch (IOException ex) {

	    				
	    			}

	    		}
				
			} else {
				
	
			}
			
		} else {
			
		}
		
		return resultString;
	}
	
	public static void writeResult(String resultAnswer, String filePath) {
		
		String filePathCreated = filePath.replaceAll("INPUT.TXT", "OUTPUT.TXT");
		
		
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;

		try {
		
			fileWriter  = new FileWriter(filePathCreated);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(resultAnswer);
			bufferedWriter.write("\n");

		} catch (IOException e) {

			

		} finally {

			try {

				if (bufferedWriter != null) {
					
					bufferedWriter.close();
				}					

				if (fileWriter  != null) {
					
					fileWriter .close();
				}
					
			} catch (IOException ex) {
				

			}

		}
		
		
	}
}
