package task557;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class generateMatrixes {
	
	public static void main(String[] args) {
		
		short[][][] matrixes = createMatrixes(130, 130, 997);
		//for(int i = 0; i < matrixes.length; i++) {
			printOneMatrix(matrixes[3]);
		//}
	}
	
	public static void printOneMatrix(short matrix[][]){
		
		System.out.println("One Matrix: ");
		
		for(int nir = 0; nir < matrix[0].length ; nir++){
			
			System.out.println("");
			
			for(int nic = 0; nic < matrix[0].length; nic ++) {
				
				System.out.print(matrix[nir][nic] + " ");
			}
		}
		
		System.out.println("");
	}
	
	private static short[][][] createMatrixes(int m, int n, int p) {
		
		short [][][] matrixes = new short [m][n][n]; // m matrixes with size n x n
		
		for(int mi = 0; mi < m; mi++){
			
			for(int nir = 0; nir < n ; nir++){
				
				for(int nic = 0; nic < n; nic ++) {
					
					matrixes[mi][nir][nic] = generateRandom(1, p);
					
				}
			}
		}
	
		return matrixes;
	}
	
	public static short generateRandom(int min, int max) {
		
		return (short) (Math.random() * ++max);
	}
	
	public static void writeResult(int result, String filePath) {
		
		String filePathCreated =  "task557_resources/OUTPUTM.TXT";
		
		
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;

		try {
			
			String resultNumber = String.valueOf(result);
			fileWriter  = new FileWriter(filePathCreated);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(resultNumber);
			bufferedWriter.write("\n");
			
			System.out.println("Result recorded into file (" + filePathCreated + ") successfully.");

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (bufferedWriter != null) {
					
					bufferedWriter.close();
				}					

				if (fileWriter  != null) {
					
					fileWriter .close();
					System.out.println("File (" + filePathCreated + ") closed successfully.\n");
				}
					
			} catch (IOException ex) {
				
				System.out.println("Error close file (" +  filePathCreated + ")\n");
				ex.printStackTrace();
			}
		}		
	}
}
