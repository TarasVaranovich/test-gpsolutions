package task557;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class task557 {
	
	//private static final String filePathExisting = "task557_resources/INPUT.TXT";

	public static void main(String[] args) {

		if(args.length != 0) { 
			
			String filePathExisting = args[0];
			int index = filePathExisting.lastIndexOf('.');
			
			if(index != -1 && filePathExisting.substring(index).equalsIgnoreCase(".TXT")){
				
				System.out.println("Matrixes program.");
				
				short[][][] resArr = readData(filePathExisting);
				
				if(resArr.length != 0){
					
					int p = resArr[resArr.length - 1][0][0];
					int a = resArr[resArr.length - 2][0][0];
					int b = resArr[resArr.length - 3][0][0];
					short[][][] newArray = new short[resArr.length - 3][resArr[0].length][resArr[0][0].length];
					System.arraycopy(resArr, 0, newArray, 0, resArr.length - 3);
					int element = getElement(newArray,p,a,b);
					writeResult(element,filePathExisting);
					
				} else {
					
					System.out.println("Program terminated.");
				}
				
			} else {
				
				System.out.println("File extension must be \".TXT\"");
			}
			
		} else {
			
			System.out.println("Write a full file path as terminal argument.");		
		}
	}
	
	public static int getElement(short matrixes[][][], int p, int a, int b){
		
		short[][] resultMatrix = new short[matrixes[0].length][matrixes[0].length];
		resultMatrix = matrixes[0];
		
		if(matrixes.length > 1){
			
			for(int mi = 1; mi < matrixes.length; mi++){
				
				resultMatrix = multiplyMatrixes(resultMatrix, matrixes[mi], p);			
			}
			
			return resultMatrix[a - 1][b - 1];
			//multiply matrix by 1
		} else {
			
			return matrixes[0][a - 1][b - 1];		
		}	
	}
	
	public static short[][] multiplyMatrixes(short [][] matrix1, short [][]matrix2, int p){
		
		int resultCell = 0;
		short[][] resultMatrix = new short[matrix1.length][matrix2.length];
		
		//fill result matrix
		for(int nir = 0; nir < matrix1.length ; nir++){
			
			for(int nic = 0; nic < matrix1.length; nic ++) {
				
				resultMatrix[nir][nic] = 0;
			}
		}
		
		for (int nir = 0; nir < matrix1.length; nir++) {//matrix 1 row
			
            for (int nic = 0; nic < matrix2[0].length; nic++) { // matrix 2 column
            	
                for (int k = 0; k < matrix1[0].length; k++) { // matrix 1 column
                	
                	resultCell += matrix1[nir][k] * matrix2[k][nic];
                	
                }

                if(resultCell >=p) {
                 	
                	resultCell = resultCell % p;
                }
                
                resultMatrix[nir][nic] = (short)resultCell;
                resultCell = 0;
            }
        }
		
		return resultMatrix;
	}
	
	public static boolean isComposite(int number){
		
		boolean isComposite = false;
		
        for (int i = 2; i < number; i++) {
        	
            if (number % i == 0) {
            	
                isComposite = true;
                
                break;
            }
        }
        
		return isComposite;
	}
	
	public static short[][][] readData(String filePath) {
		
		System.out.println("Read from:" + filePath);
		
		short[][][] falseArray = new short[0][0][0];		
		BufferedReader bufferedReader = null;
		FileReader fileReader = null;
		
		File file = new File(filePath);
		
		if (file.exists()) {
			
			try {
				
    			fileReader = new FileReader(filePath);
    			bufferedReader = new BufferedReader(fileReader);

    			String currentLine;
    			int lineNumber = 1;
    			int arraySize = 0;
    			int matrixSize = 0;
    			int arraysCounter = -1;
    			int arrayRowsCounter = 0;

    			int a = 0;
    			int b = 0;
    			int p = 0;
    			short[][][] resultArray = new short[0][0][0];			
    			
    			while ((currentLine = bufferedReader.readLine()) != null) {
   
    				if(lineNumber > 3 && lineNumber < ((matrixSize+1)*arraySize + 4)) {
   
    					if(lineNumber == (4 + (matrixSize + 1)*(arraysCounter + 1))) {
    
    						if(currentLine.trim().length() == 0) {
    
    							arraysCounter = arraysCounter + 1; 							
    							arrayRowsCounter = 0;
    							
        					} else {
        						
        						System.out.println("Error in file structure at line " + 
        											lineNumber + 
        											". Matrixes must be devided by empty lines.");
        						System.out.println("Each matrix size must be " + matrixSize + " in this case.");
        						
        						break;
        					}
    					} else {
    						
    						if(currentLine.matches("[\\d\\s?]+")){ 
    							
    							String[] membersStrings = currentLine.split(" ");
    							
    							//read one array string
    							if(membersStrings.length == matrixSize) {
    				
    								for(int i = 0; i < membersStrings.length; i++) {
    									
    									// if string to long int can't be parsed
    									if(membersStrings[i].length() < 6){
    										
    										int  numberInt = Integer.parseInt(membersStrings[i]);
    										
    										if(numberInt < p) { // less than p
    									
    											short resultMember = (short) numberInt;
    											resultArray[arraysCounter][arrayRowsCounter][i] = (short) numberInt;
    											
    										} else {
    											
    											System.out.println("Error in data at line: " + 
														lineNumber + 
														". Each array member must be less than " 
														+ p + " in this case.");
    											
    											return falseArray;
    										}
    									} else {
    										
    										System.out.println("Error in data at line: " + 
													lineNumber + 
													". Each array member must be less than " 
													+ p + ".");
    										
    										return falseArray;
    									}
    								}
    								
    								arrayRowsCounter = arrayRowsCounter + 1;
    								
    							} else {
    								
    								System.out.println("Each matrix sizes must be eqal " + matrixSize + " in this case.");
    								
    								break;
    							}
    							
     						} else {
     							
     							System.out.println("Error in data format at line " + 
										lineNumber + 
										". String can contain only digits devided by gaps.");
     							
								return falseArray;
							}
    					}
    					
    					
    				} else if(lineNumber < 4) {
 
    					switch (lineNumber) {
    					
	    					case 1:
	    						if(currentLine.matches("[\\d\\s?]+") && currentLine.length() < 8){
	    							String[] arraysData = currentLine.split(" ");
	    							if(arraysData.length == 2) {
	    								
	    								String arraySizeStr = arraysData[0];
	    								arraySize = Integer.parseInt(arraySizeStr);
	    								
	    								if(arraySize < 1 || arraySize > 130) {
	    									
	    									System.out.println("Array size must be in range 1..130.");
	    									
	    									return falseArray;
	    								}
	    								
		    							String matrixSizeStr = arraysData[1];
		    							matrixSize = Integer.parseInt(matrixSizeStr);
		    							
		    							if(matrixSize < 1 || matrixSize > 130) {
		    								
		    								System.out.println("Matrix size must be in range 1..130.");
		    								
		    								return falseArray;
		    							}
		    							//and +3 variable to store values p, a , b
		    							resultArray = new short[arraySize + 3][matrixSize][matrixSize];
		    							
	    							} else {
	    								
	    								String error = arraysData.length > 2 ? 
												"Error in data format at line " + lineNumber + ". To many variables."
												: "Error in data format at line " + lineNumber + ". Lack of data.";
	    								System.out.println(error);
	    								
	    								return falseArray;
	    							}
	    							
	    						} else {
	    							
	    							System.out.println("String can contain only two numbers "
											+ "less than 130 and one gap between.");
	    							
    								return falseArray;
	    						}
	    						
	    						break;
	    						
	    					case 2:
	    						
	    						if(currentLine.matches("[\\d\\s?]+") && currentLine.length() < 8){
	    							
	    							String[] elementData = currentLine.split(" ");
	    							
	    							if(elementData.length == 2) {
	    								
	    								String aStr = elementData[0];
	    								a = Integer.parseInt(aStr);
	    								
	    								if(a < 1 || a > matrixSize) {
	    									
	    									System.out.println("Position of number in array must be in range 1.." + 
													matrixSize + " in this case.");
	    									
	    									return falseArray;
	    								}
	    								
		    							String bStr = elementData[1];
		    							b = Integer.parseInt(bStr);
		    							
		    							if(b < 1 || b > matrixSize) {
		    								
		    								System.out.println("Position of number in array must be in range 1.." + 
													matrixSize + " in this case.");
		    								
	    									return falseArray;
		    							}
		    							
	    							} else {
	    								
	    								String errorString = elementData.length > 2 ? 
												"Error in data format at line " + lineNumber + ". To many variables."
												: "Error in data format at line " + lineNumber + ". Lack of data.";
	    								System.out.println(errorString);
	    								
	    								return falseArray;
	    							}
	    							
	    						} else {
	    							
	    							System.out.println("String can contain only two numbers less than 130 and one gap between.");
	    							
	    							return falseArray;
	    						}
	    						
	    						break;
	    						
	    					case 3:
	    						
	    						if(currentLine.matches("[\\d]+") && currentLine.length() < 5){
	    							
	    							p = Integer.parseInt(currentLine);
	    							//
	    							if(p > 1000 || p < 1 || isComposite(p)) {
	    								
	    								System.out.println("Number must be in range 0..1000.");
	    								
	    								return falseArray;
	    							} 
	    						} else {
	    							
	    							System.out.println("String can contain only digits.");
	    							
	    							return falseArray;
	    						}
	    						
	    						break;	
    					}
    					
    				} 
    				
    				if((lineNumber == ((matrixSize+1)*arraySize + 4) - 1)) {
    					
    					resultArray[resultArray.length - 1][0][0] = (short)p;
    					resultArray[resultArray.length - 2][0][0] = (short)a;
    					resultArray[resultArray.length - 3][0][0] = (short)b;
    					
    					return resultArray;
    				}
    				
    				lineNumber ++;
    			}
    			
    			if(arraysCounter < arraySize){
    				
    				System.out.println("Real metrixes count less than " + arraySize + ".");
    				
    				return falseArray;
    			}
    			

    		} catch (IOException e) {
    			

    		} finally {

    			try {

    				if (bufferedReader != null) {
    					
    					bufferedReader.close();
    					System.out.println("File (" + filePath + ") closed successfully.\n");
    				}
    					
    				if (fileReader != null) {
    					
    					fileReader.close();
    				}
    					

    			} catch (IOException ex) {
    				
    				System.out.println("Error close file (" + filePath + ")\n");
    				ex.printStackTrace();
    			}

    		}
			
			
		} else {
        	
			System.out.println("File (" + filePath +") does not exists.");
			
        }
		
		return falseArray;
	}
	
	public static void writeResult(int result, String filePath) {
		
		String filePathCreated = filePath.replaceAll("INPUT.TXT", "OUTPUT.TXT");
		
		
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;

		try {
			
			String resultNumber = String.valueOf(result);
			fileWriter  = new FileWriter(filePathCreated);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(resultNumber);
			bufferedWriter.write("\n");
			
			System.out.println("Result recorded into file (" + filePathCreated + ") successfully.");

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (bufferedWriter != null) {
					
					bufferedWriter.close();
				}					

				if (fileWriter  != null) {
					
					fileWriter .close();
					System.out.println("File (" + filePathCreated + ") closed successfully.\n");
				}
					
			} catch (IOException ex) {
				
				System.out.println("Error close file (" +  filePathCreated + ")\n");
				ex.printStackTrace();
			}
		}		
	}
	
	//below are service methods for development only
	
	/*private static short[][][] createMatrixes(int m, int n, int p) {
	
		short [][][] matrixes = new short [m][n][n]; // m matrixes with size n x n
		
		for(int mi = 0; mi < m; mi++){
			
			for(int nir = 0; nir < n ; nir++){
				
				for(int nic = 0; nic < n; nic ++) {
					
					matrixes[mi][nir][nic] = generateRandom(1, p);
					
				}
			}
		}
	
		return matrixes;
	}
	
	public static void printMatrixes(short matrixes[][][]) {
	
	for(int mi = 0; mi < matrixes.length; mi++){
		
			System.out.println("Matrix " + mi + ":");
			
			for(int nir = 0; nir < matrixes[0].length ; nir++){
				
				System.out.println("");
				
				for(int nic = 0; nic < matrixes[0][0].length; nic ++) {
					
					System.out.print(matrixes[mi][nir][nic] + " ");
				}
			}
			
			System.out.println("");
		}
	}
	
	public static void printOneMatrix(short matrix[][]){
		
		System.out.println("One Matrix: ");
		
		for(int nir = 0; nir < matrix[0].length ; nir++){
			
			System.out.println("");
			
			for(int nic = 0; nic < matrix[0].length; nic ++) {
				
				System.out.print(matrix[nir][nic] + " ");
			}
		}
		
		System.out.println("");
	}
	
	public static short generateRandom(int min, int max) {
		
		return (short) (Math.random() * ++max);
	}*/
}
