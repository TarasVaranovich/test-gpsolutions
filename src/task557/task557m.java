package task557;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class task557m {

	public static void main(String[] args) {
		//task557_resources/
		short[][][] resArr = readData("input.txt");

		if(resArr.length > 0) {
			
			int p = resArr[resArr.length - 1][0][0];
			int a = resArr[resArr.length - 2][0][0];
			int b = resArr[resArr.length - 3][0][0];
			short[][][] newArray = new short[resArr.length - 3][resArr[0].length][resArr[0][0].length];
			System.arraycopy(resArr, 0, newArray, 0, resArr.length - 3);
			int element = getElement(newArray,p,a,b);
			//System.out.println(element);
			writeResult(element,"output.txt");

		} 						
	}
	
	public static int getElement(short matrixes[][][], int p, int a, int b){
		
		short[][] resultMatrix = new short[matrixes[0].length][matrixes[0].length];
		resultMatrix = matrixes[0];
		if(matrixes.length > 1){
			for(int mi = 1; mi < matrixes.length; mi++){
				
				resultMatrix = multiplyMatrixes(resultMatrix, matrixes[mi], p);			
			}
			
			return resultMatrix[a - 1][b - 1];
			
		} else {
			
			return matrixes[0][a - 1][b - 1];		
		}		
	}
	
	public static short[][] multiplyMatrixes(short [][] matrix1, short [][]matrix2, int p){
		
		int resultCell = 0;
		short[][] resultMatrix = new short[matrix1.length][matrix2.length];
		
		for(int nir = 0; nir < matrix1.length ; nir++){
			
			for(int nic = 0; nic < matrix1.length; nic ++) {
				
				resultMatrix[nir][nic] = 0;
			}
		}
		
		for (int nir = 0; nir < matrix1.length; nir++) {//matrix 1 row
			
            for (int nic = 0; nic < matrix2[0].length; nic++) { // matrix 2 column
            	
                for (int k = 0; k < matrix1[0].length; k++) { // matrix 1 column
                	
                	resultCell += matrix1[nir][k] * matrix2[k][nic];
                	
                }

                if(resultCell >=p) {
                 	
                	resultCell = resultCell % p;
                }
                
                resultMatrix[nir][nic] = (short)resultCell;
                resultCell = 0;
            }
        }
		
		return resultMatrix;
	}
	
	/*public static short generateRandom(int min, int max) {
		
		return (short) (Math.random() * ++max);
	}*/
	
	public static boolean isComposite(int number){
		
		boolean isComposite = false;
		
        for (int i = 2; i < number; i++) {
        	
            if (number % i == 0) {
            	
                isComposite = true;
                
                break;
            }
        }
        
		return isComposite;
	}
	
	public static short[][][] readData(String filePath) {
		
		short[][][] falseArray = new short[0][0][0];
		
		BufferedReader bufferedReader = null;
		FileReader fileReader = null;
		
		File file = new File(filePath);
		
		if (file.exists()) {
			
			try {
				
    			fileReader = new FileReader(filePath);
    			bufferedReader = new BufferedReader(fileReader);

    			String currentLine;
    			int lineNumber = 1;
    			int arraySize = 0;
    			int matrixSize = 0;
    			int arraysCounter = -1;
    			int arrayRowsCounter = 0;

    			int a = 0;
    			int b = 0;
    			int p = 0;
    			short[][][] resultArray = new short[0][0][0];			
    			
    			while ((currentLine = bufferedReader.readLine()) != null) {
   
    				if(lineNumber > 3 && lineNumber < ((matrixSize+1)*arraySize + 4)) {
   
    					if(lineNumber == (4 + (matrixSize + 1)*(arraysCounter + 1))) {
    
    						if(currentLine.trim().length() == 0) {
    
    							arraysCounter = arraysCounter + 1; 							
    							arrayRowsCounter = 0;
    							
        					} else {
        						//System.out.println("P1");
        						break;
        					}
    					} else {
    						
    						if(currentLine.matches("[\\d\\s?]+")){ 
    							
    							String[] membersStrings = currentLine.split(" ");
    							
    							//read one array string
    							if(membersStrings.length == matrixSize) {
    				
    								for(int i = 0; i < membersStrings.length; i++) {
    									
    									// if string to long int can't be parsed
    									if(membersStrings[i].length() < 6){
    										
    										int  numberInt = Integer.parseInt(membersStrings[i]);
    										
    										if(numberInt < p) { // less than p
    									
    											short resultMember = (short) numberInt;
    											resultArray[arraysCounter][arrayRowsCounter][i] = (short) numberInt;
    											
    										} else {
    											//System.out.println("P2" + numberInt);
    											return falseArray;
    										}
    									} else {
    										//System.out.println("P3");
    										return falseArray;
    									}
    								}
    								
    								arrayRowsCounter = arrayRowsCounter + 1;
    								
    							} else {
    								//System.out.println("P4");
    								break;
    							}
    							
     						} else {
     							//System.out.println("P5");
								return falseArray;
							}
    					}
    					
    					
    				} else if(lineNumber < 4) {
 
    					switch (lineNumber) {
    					
	    					case 1:
	    						if(currentLine.matches("[\\d\\s?]+") && currentLine.length() < 8){
	    							String[] arraysData = currentLine.split(" ");
	    							if(arraysData.length == 2) {
	    								
	    								String arraySizeStr = arraysData[0];
	    								arraySize = Integer.parseInt(arraySizeStr);
	    								
	    								if(arraySize < 1 || arraySize > 130) {
	    									
	    									//System.out.println("P6");
	    									return falseArray;
	    								}
	    								
		    							String matrixSizeStr = arraysData[1];
		    							matrixSize = Integer.parseInt(matrixSizeStr);
		    							
		    							if(matrixSize < 1 || matrixSize > 130) {
		    								//System.out.println("P7");
		    								return falseArray;
		    							}
		    							//and +3 variable to store values p, a , b
		    							resultArray = new short[arraySize + 3][matrixSize][matrixSize];
		    							
	    							} else {
	    								//System.out.println("P8");
	    								return falseArray;
	    							}
	    							
	    						} else {
	    							//System.out.println("P9");
    								return falseArray;
	    						}
	    						
	    						break;
	    						
	    					case 2:
	    						
	    						if(currentLine.matches("[\\d\\s?]+") && currentLine.length() < 8){
	    							
	    							String[] elementData = currentLine.split(" ");
	    							
	    							if(elementData.length == 2) {
	    								
	    								String aStr = elementData[0];
	    								a = Integer.parseInt(aStr);
	    								
	    								if(a < 1 || a > matrixSize) {
	    									//System.out.println("P10");
	    									return falseArray;
	    								}
	    								
		    							String bStr = elementData[1];
		    							b = Integer.parseInt(bStr);
		    							
		    							if(b < 1 || b > matrixSize) {
		    								//System.out.println("P11");
	    									return falseArray;
		    							}
		    							
	    							} else {
	    								//System.out.println("P12");
	    								return falseArray;
	    							}
	    							
	    						} else {
	    							//System.out.println("P13");
	    							return falseArray;
	    						}
	    						
	    						break;
	    						
	    					case 3:
	    						
	    						if(currentLine.matches("[\\d]+") && currentLine.length() < 5){
	    							
	    							p = Integer.parseInt(currentLine);
	    							//
	    							if(p > 1000 || p < 1 || isComposite(p)) {
	    								//System.out.println("P14");
	    								return falseArray;
	    							} 
	    						} else {
	    							//System.out.println("P15");
	    							return falseArray;
	    						}
	    						
	    						break;	
    					}
    					
    				} 
    				
    				if((lineNumber == ((matrixSize+1)*arraySize + 4) - 1)) {
    					
    					resultArray[resultArray.length - 1][0][0] = (short)p;
    					resultArray[resultArray.length - 2][0][0] = (short)a;
    					resultArray[resultArray.length - 3][0][0] = (short)b;
    					
    					return resultArray;
    				}
    				
    				lineNumber ++;
    			}
    			
    			if(arraysCounter < arraySize){
    				//System.out.println("P6");
    				return falseArray; //no matter
    			}
    			

    		} catch (IOException e) {
    			

    		} finally {

    			try {

    				if (bufferedReader != null) {
    					
    					bufferedReader.close();
    				}
    					
    				if (fileReader != null) {
    					
    					fileReader.close();
    				}
    					

    			} catch (IOException ex) {

    			}

    		}
			
			
		} else {
        	
        }
		
		return falseArray;
	}
	
	public static void writeResult(int result, String filePath) {
		
		String filePathCreated = filePath.replaceAll("INPUT.TXT", "OUTPUT.TXT");
		
		
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;

		try {
			
			String resultNumber = String.valueOf(result);
			fileWriter  = new FileWriter(filePathCreated);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(resultNumber);
			bufferedWriter.write("\n");
			

		} catch (IOException e) {


		} finally {

			try {

				if (bufferedWriter != null) {
					
					bufferedWriter.close();
				}					

				if (fileWriter  != null) {
					
					fileWriter .close();
				}
					
			} catch (IOException ex) {
	
				
			}
		}		
	}
}


