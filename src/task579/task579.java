package task579;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
import java.util.Arrays;

public class task579 {
	
	//private static final String filePathExisting = "task579_resources/INPUT.TXT";
	
	public static void main(String[] args) {
		
		if(args.length != 0) {
			
			String filePathExisting = args[0];
			int index = filePathExisting.lastIndexOf('.');
			
			if(index != -1 && filePathExisting.substring(index).equalsIgnoreCase(".TXT")){
				
				System.out.println("Sum module program.");
				
				short[] newArray = readData(filePathExisting);
				
				if(newArray.length != 0) {
					
					short[] sequence = getSequence(newArray);
					writeResult(sequence, filePathExisting);
					
				} else {
					
					System.out.println("Program terminated.");
				}
				
			} else {
				
				System.out.println("File extension must be \".TXT\"");
			}
			
		} else {
			
			System.out.println("Write a full file path as terminal argument.");
			
		}
	}
	
	public static short[] getSequence(short[] numbers){
		
		int n = numbers.length;
		int positiveSum = 0;
		int negativeSum = 0;
		
		short[] Indexes = new short[n];
		int directCounter = 0;
		int reverseCounter = n - 1;
	
		for(int i = 0; i < n; i ++) {
			
			if(numbers[i] >= 0) {
				
				positiveSum = positiveSum + numbers[i];
				Indexes[directCounter] = (short)i;
				directCounter = directCounter + 1;
			
				
			} else {
				
				negativeSum = negativeSum + numbers[i];
				Indexes[reverseCounter] = (short)i;
				reverseCounter = reverseCounter - 1;
	
			}

		}
		
		//"...выбранной вами подпоследовательности...". Из двух равных я выбираю положительную"
		if(positiveSum >= negativeSum * (-1)){
			
			short[] result = new short[directCounter];
			
			for(int z = 0; z < directCounter; z++) {
				
				short index = Indexes[z];
				index++;
				result[z] = index;
				
			}
			
			return result;
			
		} else{
			
			short[] result = new short[n - directCounter];
			int z = n - 1;
			int k = 0;
			
			while(z > reverseCounter){
				
				short index = Indexes[z];
				index++;
				result[k] = index; 
				z--;
				k++;
			}
			
			return result;
		}
		
	}
	
	public static short[] readData(String filePath){
		
		int n = 0;
		System.out.println("Read from:" + filePath);
		BufferedReader bufferedReader = null;
		FileReader fileReader = null;
		short[] falseArray = new short[0];
		
		File file = new File(filePath);
		
        if (file.exists()) {
        	
        	try {
				
    			fileReader = new FileReader(filePath);
    			bufferedReader = new BufferedReader(fileReader);

    			String currentLine;
    			int lineNumber = 1;
    			
    			while ((currentLine = bufferedReader.readLine()) != null) {
    				
    				if(lineNumber == 1) {
    					
    					if(currentLine.matches("[\\d]+")){
    						
    						if(currentLine.length() < 6) {
    							
    							n = Integer.parseInt(currentLine);
        						
        						if(!(1 <= n && n <= 10000)) {
        							
        							System.out.println("Count of array numbers must be in range 1..10000.\n");
        					
        						}
        						
    						} else {
    							
    							System.out.println("Count of array numbers must be in range 1..10000.\n");
    						}
    												
    					} else {
    						
    						System.out.println("Wrong data in file line 1.");
    						
    					}
    				} else if(lineNumber == 2){
    					
    					if(currentLine.matches("[\\d\\s?\\-?]+")) {
    						
    						String[] stringsArr = currentLine.split(" ");
    						
    						if(stringsArr.length == n) {
    							
    							short[] numbersR = new short[n];
    							
    							for(int i = 0; i < stringsArr.length; i++) {
    								
    								if(stringsArr[i].length() < 7) {
    									
    									int  numberInt = Integer.parseInt(stringsArr[i]);
    									numbersR[i] = (short) numberInt;
        								 
        								if(Math.abs(numbersR[i]) > 10000){
        									 
        									System.out.println("Absolute value of number " + 
        														String.valueOf(numberInt) + 
        														" more than 10000.\n");
        									return falseArray;
        								}
        								
    								} else {
    									
    									System.out.println("Absolute value of number " + 
    														stringsArr[i] + 
															" more than 10000.\n");
    									return falseArray;
    								}
    												
    							}
    							
    							return numbersR;
    							
    						} else {
    							
    							System.out.println("Array numbers count not equal predetermined count");
    							
    						}
    		
    					} else {
    						
    						System.out.println("Wrong data in file line 2.");
    						
    					}
    				} else {
    					
    					break;
    				}
    				
    				lineNumber ++;
    			}
    		

    		} catch (IOException e) {
    			
    			System.out.println("Error read from file (" + filePath + ")\n");
    			e.printStackTrace();

    		} finally {

    			try {

    				if (bufferedReader != null) {
    					
    					bufferedReader.close();
    				}
    					
    				if (fileReader != null) {
    					
    					fileReader.close();
    					System.out.println("File (" + filePath + ") closed successfully.\n");
    				}
    					

    			} catch (IOException ex) {

    				
    				System.out.println("Error close file (" + filePath + ")\n");
    				ex.printStackTrace();
    			}

    		}
        	 	
        } else {
        	
        	System.out.println("File (" + filePath +") does not exists.");
        }
	
		return falseArray;
	}
	
	public static void writeResult(short[] result, String filePath) {
		
		String filePathCreated = filePath.replaceAll("INPUT.TXT", "OUTPUT.TXT");
		
		
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;

		try {

			String numberCount = String.valueOf(result.length) ;
			String resultArray = Arrays.toString(result);
			resultArray = resultArray.replaceAll(",|\\[|\\]", "");
			fileWriter  = new FileWriter(filePathCreated);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(numberCount);
			bufferedWriter.write("\n");
			bufferedWriter.write(resultArray);
			
			System.out.println("Result recorded into file (" + filePathCreated + ") successfully.");

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (bufferedWriter != null) {
					
					bufferedWriter.close();
				}					

				if (fileWriter  != null) {
					
					fileWriter .close();
					System.out.println("File (" + filePathCreated + ") closed successfully.\n");
				}
					
			} catch (IOException ex) {
				
				System.out.println("Error close file (" +  filePathCreated + ")\n");
				ex.printStackTrace();
			}

		}
	}
}
