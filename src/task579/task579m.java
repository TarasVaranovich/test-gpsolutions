package task579;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
import java.util.Arrays;

public class task579m {
	
	public static void main(String[] args) {
		
				
		short[] newArray = readData("input.txt");
				
		if(newArray.length != 0) {
					
			short[] sequence = getSequence(newArray);
			writeResult(sequence, "output.txt");
					
		} 
	}
	
	public static short[] getSequence(short[] numbers){
		
		int n = numbers.length;
		int positiveSum = 0;
		int negativeSum = 0;
		
		short[] Indexes = new short[n];
		int directCounter = 0;
		int reverseCounter = n - 1;
	
		for(int i = 0; i < n; i ++) {
			
			if(numbers[i] >= 0) {
				
				positiveSum = positiveSum + numbers[i];
				Indexes[directCounter] = (short)i;
				directCounter = directCounter + 1;
			
				
			} else {
				
				negativeSum = negativeSum + numbers[i];
				Indexes[reverseCounter] = (short)i;
				reverseCounter = reverseCounter - 1;
	
			}

		}
		
		if(positiveSum >= negativeSum * (-1)){
			
			short[] result = new short[directCounter];
			
			for(int z = 0; z < directCounter; z++) {
				
				short index = Indexes[z];
				index++;
				result[z] = index;
				
			}
			
			return result;
			
		} else{
			
			short[] result = new short[n - directCounter];
			int z = n - 1;
			int k = 0;
			
			while(z > reverseCounter){
				
				short index = Indexes[z];
				index++;
				result[k] = index; 
				z--;
				k++;
			}
			
			return result;
		}
		
	}
	
	public static short[] readData(String filePath){
		
		int n = 0;
		BufferedReader bufferedReader = null;
		FileReader fileReader = null;
		short[] falseArray = new short[0];
		
		File file = new File(filePath);
		
        if (file.exists()) {
        	
        	try {
				
    			fileReader = new FileReader(filePath);
    			bufferedReader = new BufferedReader(fileReader);

    			String currentLine;
    			int lineNumber = 1;
    			
    			while ((currentLine = bufferedReader.readLine()) != null) {
    				
    				if(lineNumber == 1) {
    					
    					if(currentLine.matches("[\\d]+")){
    						
    						if(currentLine.length() < 6) {
    							
    							n = Integer.parseInt(currentLine);
        						
        						if(!(1 <= n && n <= 10000)) {
        					
        						}
        						
    						} else {
    							
    						}
    												
    					} else {
    
    						
    					}
    				} else if(lineNumber == 2){
    					
    					if(currentLine.matches("[\\d\\s?\\-?]+")) {
    						
    						String[] stringsArr = currentLine.split(" ");
    						
    						if(stringsArr.length == n) {
    							
    							short[] numbersR = new short[n];
    							
    							for(int i = 0; i < stringsArr.length; i++) {
    								
    								if(stringsArr[i].length() < 7) {
    									
    									int  numberInt = Integer.parseInt(stringsArr[i]);
    									numbersR[i] = (short) numberInt;
        								 
        								if(Math.abs(numbersR[i]) > 10000){
        									
        									return falseArray;
        								}
        								
    								} else {
    									
 
    									return falseArray;
    								}
    												
    							}
    							
    							return numbersR;
    							
    						} else {
    							
    							
    						}
    		
    					} else {
    						
    						
    					}
    				} else {
    					
    					break;
    				}
    				
    				lineNumber ++;
    			}
    		

    		} catch (IOException e) {
    			

    		} finally {

    			try {

    				if (bufferedReader != null) {
    					
    					bufferedReader.close();
    				}
    					
    				if (fileReader != null) {
    					
    					fileReader.close();
    				}
    					

    			} catch (IOException ex) {

    			}

    		}
        	 	
        } else {
        	
        }
	
		return falseArray;
	}
	
	public static void writeResult(short[] result, String filePath) {
		
		String filePathCreated = filePath.replaceAll("INPUT.TXT", "OUTPUT.TXT");
		
		
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;

		try {

			String numberCount = String.valueOf(result.length) ;
			String resultArray = Arrays.toString(result);
			resultArray = resultArray.replaceAll(",|\\[|\\]", "");
			fileWriter  = new FileWriter(filePathCreated);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(numberCount);
			bufferedWriter.write("\n");
			bufferedWriter.write(resultArray);
			

		} catch (IOException e) {


		} finally {

			try {

				if (bufferedWriter != null) {
					
					bufferedWriter.close();
				}					

				if (fileWriter  != null) {
					
					fileWriter .close();
				}
					
			} catch (IOException ex) {
				
			}

		}
	}
}
