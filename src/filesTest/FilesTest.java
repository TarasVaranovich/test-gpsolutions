package filesTest;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.FileWriter;

public class FilesTest {
	
	
	private static final String filePathExist = "files_resources/INPUT.TXT";
	private static final String filePathCreated = "files_resources/OUTPUT.TXT";
	
	public static void main(String[] args) {
		
		readFromFile(filePathExist);
		writeIntoFile(filePathCreated);
		
	}
	
	public static void readFromFile(String FILENAME){
		
		BufferedReader br = null;
		FileReader fr = null;

		try {

			//br = new BufferedReader(new FileReader(FILENAME));
			fr = new FileReader(FILENAME);
			br = new BufferedReader(fr);

			String sCurrentLine;
			int lineNumber = 1;
			while ((sCurrentLine = br.readLine()) != null) {
				
				if(sCurrentLine.trim().length() == 0) {
					
					System.out.println("Empty line" + lineNumber);
				} 
				
				if(sCurrentLine.matches("[\\d]+")){
					
					System.out.println("Digits " +  lineNumber + " :" + sCurrentLine);				
				} 
				
				if(sCurrentLine.matches("[\\d]+")){
					
					System.out.println("Digits and One whitespace "+  lineNumber +" :");				
				} 
				
				if(true){
					
					System.out.println("Text:" + sCurrentLine);
				}
				
				lineNumber++;
			}
			
			//System.out.println("Read only first line...");
			//String line = br.readLine();
			//System.out.println(line);

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (br != null)
					br.close();

				if (fr != null)
					fr.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}

		}

	}
	
	public static void writeIntoFile(String FILENAME){
		
		BufferedWriter bw = null;
		FileWriter fw = null;

		try {

			String content = "Changed content\n";

			fw = new FileWriter(FILENAME);
			bw = new BufferedWriter(fw);
			bw.write(content);

			System.out.println("Done");

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}

		}
	}
}
